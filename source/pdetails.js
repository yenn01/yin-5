function quantity()
{
    for (var i=1; i <= 10; i++)
    {
        document.write("<option>" + i + "</option>");
    }
}

function size(){
var header = document.getElementById("product-size");
var btns = header.getElementsByClassName("psize-selected");
for (var i = 0; i < btns.length; i++) 
{
    btns[i].addEventListener("click", function() 
    {
        var current = document.getElementsByClassName("active");
        if (current.length > 0) 
        { 
            current[0].className = current[0].className.replace(" active", "");
        }
        this.className += " active";
    });
}
}